#include "logica.h"
/*
	Esta funcion se encarga de selecionar una ficha, moverla y modificar el tablero en consecuencia de esa jugada.
	Si se selecciona una ficha y se quiere cambiar se puede volver para atras
*/
void jugada (int *tab, int J)
{
	int selec, mov, jugar, x, y , xf, yf;	
	do 
	{
		do	
		{
			printf ("elegi la ficha en (X;Y) : ");
			scanf ("%d %d",&x,&y);
			y--;
			x--;
			selec = seleccionar (tab, &x, &y, J);
		} while (selec ==1); 	//elijo la ficha, no sale hasta elegir una ficha bien
	
		do	
		{
			printf ("mover a (X;Y): ");
			scanf ("%d %d",&xf,&yf);
			xf--;
			yf--;
			mov = mover (tab, &xf, &yf, x, y, J);
		} while (mov ==-1);	//elijo a donde la muevo, si no hago un movimiento valido no salgo

		if(mov == 0){jugar = 1;} // si mov es 0 entonces sigo en la jugada porque voy a elegir otra ficha
		else if (mov == 1){jugar = 0;} // si mov es 1 entonces salgo
	} while (jugar) ; 	// no sale hasta haber selecionado y movido bien una ficha

	consecuencia(tab ,xf,yf, J); // al terminar de selecionar y mover se modifica el tablero con la consecuencia de la jugada
}

