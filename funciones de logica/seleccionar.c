#include "logica.h"
/*
	Esta funcion se encarga de ver si en la coordenada que ingresaste hay una ficha perteneciente a tu jugador.
	RETORNA: 0 en caso de seleccionar bien y 1 en caso de error. 
*/
int seleccionar(int *tab,int *x, int *y, int J)
{ 

	if ( *(tab+X*(*y)+*x) != J )   // verifico si la casilla elegida contiene una ficha del jugador
	{
		return 1;	//Ficha incorrecta
	}
	else
	{
		return 0;
	} 
}
