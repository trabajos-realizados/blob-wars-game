#include "logica.h"
/*
	Esta funcion se encarga de inicializar el tablero del tamaño correspondiente de filas y columnas 
	y ubica las fichas de los jugadores en las esquinas
*/
void init(int *tab)
{
	int auxx, auxy;

	for(auxy=0;auxy<Y;auxy++)	//Inicializo el tablero
	{
		for(auxx=0;auxx<X;auxx++)
		{
			*(tab+X*auxy + auxx)=VACIO;
		}
	}

	*(tab+X*0 + 0)=J1;	//Se establecen jugadores
	*(tab+X*(Y-1) + 0)=J1;
	*(tab+X*1  -1 )=J2;
	*(tab+X*(Y-1) + (X-1) )=J2;
} 
