#include "logica.h"	
/*
	Esta funcion se encarga de revisar el estado del tablero para saber si seguir el juego o si ya termino.
	RETORNA:	0 en caso de seguir en juego
			1 en caso de que gano el jugador 1
			2 en caso de que gano el jugador 2
			3 en caso de que empataron
*/
int condicion(int *tab)
{
	int auxx,auxy;	//Para moverme en la matriz
	int p1=0,p2=0, va = 0;	//Contador de fichas de jugadores y casillas vacias
	
	for(auxx=0;auxx<X;auxx++)	
	{
		for(auxy=0;auxy<Y;auxy++)
		{
			if( *(tab+X*auxy + auxx )== J1)	//Cuento fichas de jugador 1
			{
				p1++;
			}
			if( *(tab+X*auxy + auxx )== J2)	//Cuento fichas de jugador 2
			{
				p2++;
			}
			else if ( *(tab+X*auxy + auxx )== VACIO) //cuento las casillas vacias
			{
				va++;
			}
		}
	}
	
	//VERIFICACION
	if ( va != 0 && p2 != 0 && p1 != 0 ) // si hay al menos 1 casilla vacia y 1 ficha de cada jugador entonces sigo en juego
	{	
		return EN_JUEGO;	
	}
	if(p1==p2) // si habian 0 casillas vacias y quedaron con la misma cantidad es empate
	{
		return EMPATE;
	}
	if( (p1<p2) || (p1 == 0) ) // si el jugador 1 tiene menos fichas o se quedo sin fichas gano el jugador 2
	{
		return GANO_J2;
	}
	if( (p1>p2) || (p2 == 0) ) // si el jugador 2 tiene menos fichas o se quedo sin fichas gano el jugador 1
	{
		return GANO_J1;
	}
} 
