#include "logica.h"
/*
	Esta funcion se encarga de convertir las fichas que no sean del jugador y esten alrededor de la ficha
	que acaba de mover
*/

void consecuencia(int *tab, int xf, int yf, int J)
{
	int A;
 	if (xf < (X-1))							//verifico si queda espacio en la direccion a la que me voy a mover
 	{
		A = X*(yf) + xf+1;					//me muevo a la derecha
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	//si no esta la ficha del jugador y tampoco es vacia cambio la ficha
	}
	if (yf < (Y-1))
	{	
		A = X*(yf+1) + xf ;					//me muevo hacia abajo
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;} 	
	}
	if (xf > 0 )
	{
		A = X*(yf) + xf-1;					//me muevo hacia la izquierda
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}   
	}
	if (yf > 0 )
	{
		A =X*(yf-1) + xf;					//me muevo hacia arriba
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
	if (xf < (X-1) && yf < (Y-1))
 	{
		A =X*(yf+1) + (xf+1);					 //me muevo hacia la diagonal inf derecha
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}   
	}
 	if (xf > 0 && yf > 0) 
	{	
		A =X*(yf-1) + (xf-1);					//me muevo en diagonal sup-iz
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
	if (xf > 0 && yf < (Y-1))
	{
		A = X*(yf+1) + (xf-1);					//me muevo en la diagonal inf iz
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
	if (xf < (X-1) && yf > 0)
	{
		A = X*(yf-1) + (xf+1);					//me muevo en la diagonal sup derecha
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
} 
