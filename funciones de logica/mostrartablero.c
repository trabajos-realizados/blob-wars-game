#include "logica.h"
/*
	Imprime el estado del tablero
*/
void MostrarTablero (int *tab)
{
	int auxx,auxy;

	for(auxy=0;auxy<Y;auxy++)	//Recorro el tablero
	{
		for(auxx=0;auxx<X;auxx++)
		{
			printf ("%d", *(tab+X*auxy + auxx)); 
		}
		printf ("\n");
	}
}
