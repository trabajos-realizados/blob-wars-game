#include "prueba.h"

int main()
{
	int tablero[Y][X];
	int jugador, turno;
	jugador= 1;
	turno= 1;
 
	printf ("partida en tablero de %d x %d \n", X, Y);	

	init(&tablero[0][0]);         				//inicializo el tablero con las fichas en las pos. iniciales

	MostrarTablero(&tablero[0][0]);				//muestra el tablero inicial

	printf ("turno de jugador 1 \n");

	while ((condicion(&tablero[0][0])==EN_JUEGO))  		// mientras ningun jugador haya ganado entro al while
	{
		jugada(&tablero[0][0], jugador);		// realiza un turno del jugador
	
		MostrarTablero(&tablero[0][0]);			//imprime el tablero luego de una jugada

		CambiarTurno(&tablero[0][0], &jugador, &turno); //si sigue el juego, cambia al otro jugador
	}
	finalizar(&tablero[0][0]);				//revisa la condicion para saber como termino el juego

	return 0; 

}
