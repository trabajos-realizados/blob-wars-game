#include "logica.h"
/*
	Mueve la ficha seleccionada.
	RETORNA:	-1 si no es posible el movimiento
			0 si se selecciona moverla al mismo lugar donde esta (esto es valido para elegir mover otra ficha)
			1 si se movio correctamente la ficha   
*/
int mover(int *tab, int *xf, int *yf, int x, int y, int J)
{
	if (abs(*xf -x) == 0 && abs(*yf -y) == 0) //si elige mover la ficha al mismo luegar puede cambiar de ficha
	{
		printf ("elegi otra ficha \n");
		return 0;
	}
	if ( *(tab+X*(*yf)+*xf) == VACIO && (*xf < X) && (*yf < Y) && (*xf >= 0) && (*yf >= 0)  )   
	{	
		if (( abs(*xf -x) < 3 ) && ( abs(*yf -y) < 3) )  //verifico que este en el rango de 2 casillas
		{	
			*(tab+X*(*yf)+*xf) = J;			//imprimo la ficha en el lugar que indico

			if ( abs(*xf -x) == 2 || abs(*yf -y) == 2) //si se movio a 2 de distancia borro la original, sino la dejo
			{	
				*(tab+X*(y)+x) = VACIO;
			}
			
			return 1;
		}
	}
	else 
		{ 
			printf ("move bien wacho \n");  //si anduvo mal le indico que hizo mal el movimiento 
			return -1;		
		}
	
}
