#include "cliente.h"

#define JUGADOR_ACTUAL	J2

int conexion(int puerto, struct sockaddr_in *configuracion)
{
	int fd_cliente;

	//CREO EL SOCKET
	fd_cliente=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(fd_cliente==-1)
	{
		perror("ERROR SOCKET: ");
		return -1;
	}

	//RELLENO LA ESTRUCTURA CON DATOS DEL SERVIDOR
	//configuracion.sin_addr.s_addr=inet_addr(IP);	//Direccion local
	configuracion->sin_family=AF_INET;		//Familia de direciones
	configuracion->sin_port=htons(puerto);		//Se designa el puerto al que me quiero conectar
	memset(configuracion->sin_zero,0,8);		//Se rellena la structura

	return fd_cliente;
}
