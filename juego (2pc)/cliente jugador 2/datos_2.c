#include "cliente.h"
#include "logica.h" 
#include "funciones_allegro.h"
#define JUGADOR_ACTUAL	J2
int datos(int sockaddr)
{
	ALLEGRO_DISPLAY *display;
	int tablero[X][Y];
	int aux,i, tam_send, tam_recv;

	if(!al_init())
	{
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}
	display = al_create_display(WITH, HIGH);
	if(!display)
	{
	      	perror("\nERROR DE PISPLAY: ");
	      	return -1;
   	}
	al_init_primitives_addon();
	if(!al_install_keyboard())
	{
		fprintf(stderr, "failed to initialize the keyboard!\n");
		return -1;
	}
		init(tablero[0]);
		MostrarTablero (tablero[0]);
		crear_tablero();
	 			
		act_fichas(&tablero[0][0]);				//muestra el tablero inicial
	
		al_flip_display();					//refresco la pantasha 
	while((condicion(&tablero[0][0], JUGADOR_ACTUAL)==EN_JUEGO))
	{
		//ESPERO RESPUESTA
		printf("\n ESPERANDO RESPUESTA DE SERVIDOR... \n");
		tam_recv=recv(sockaddr,tablero,sizeof(tablero),0);
		if(tam_recv>0)
		{
			crear_tablero();
	 			
			act_fichas(&tablero[0][0]);				//muestra el tablero inicial
	
			al_flip_display();					//refresco la pantasha 
					
		}
		if(tam_recv<0)
		{
			perror("ERROR RECV: ");
			return -1;
		}
		if ((condicion(&tablero[0][0], JUGADOR_ACTUAL)==EN_JUEGO))
		{
		//	CambiarTurno(&tablero[0][0], &jugador, &turno);
			act_jugada(tablero[0],display, JUGADOR_ACTUAL);

			crear_tablero();
			act_fichas(tablero[0]);
			al_flip_display();
			MostrarTablero (tablero[0]);	
			//ENVIO DATOS
			tam_send=send(sockaddr,tablero,sizeof(tablero),0);
			if(tam_send>0)
			{
				printf("\nENVIADO");
			}
			if(tam_send<0)
			{
				perror("ERROR SEND: ");
				return -1;
			}
		}

	}
	
	al_finalizar(tablero[0], JUGADOR_ACTUAL);	
	
	al_shutdown_primitives_addon();	

	al_destroy_display(display);
		
	return 0;
}
