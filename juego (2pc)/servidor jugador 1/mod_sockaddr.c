#include "servidor.h"

////////////MOD_SOCKADDR///////////////////////////////////////////////////////////////////////////
int mod_sockaddr(int *port, struct sockaddr_in *config)
{
	if((*port)<MAXPORT)
	{
		(*port)++;	//Incremento el puerto	hasta encontrar un desocupado
		//RELLENO LA ESTRUCTURA
		config->sin_port=htons(*port);   //Eligo el puerto
		memset(config->sin_zero,0,8);	   //Se rellena a 0 para preservar tamaño original de struct sockadr
	}
	else
	{
		return -1;
	}

	return 1;
}
