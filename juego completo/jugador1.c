#include "servidor.h"
#include "logica.h"
#include "funciones_allegro.h"

int jugador_1(int cant, char **arg)
{
	int tam_send, tam_recv ,puerto;
	int condicion=1;	//Para el while

	int fd_servidor, fd_cliente;
	int size_addr=sizeof(struct sockaddr_in);
	struct sockaddr_in servidor, cliente;
	
	//CREO SOCKET
	fd_servidor=sock_servidor(&puerto , &servidor );

	printf("Esperando conexión por el puerto %d\n",puerto);
	
	//ACEPTO CONEXION
	fd_cliente=accept(fd_servidor,(struct sockaddr*)&cliente, &size_addr);
	if(fd_cliente==-1)
	{
		perror("ERROR DE ACCEPT: ");
		return -1;
	}

	//INTERCAMBIO DE DATOS//
	while(condicion)
	{
		condicion=datos_1(fd_cliente);
	}

	printf("FIN\n");
	close(fd_cliente);

	return 0;
}
