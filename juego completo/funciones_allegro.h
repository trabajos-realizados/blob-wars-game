#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include "logica.h"
//******SELECTOR*************
#define TIME 0.1
#define SELECTOR (WITH/(X*3) + 4)
#define SELEC_ESP 5
//Color
#define SEL_R	255
#define SEL_G	255
#define SEL_B	255
//color de ficha seleccionada
#define ELEG_R	0
#define ELEG_G	255
#define ELEG_B	255


//******TECLADO**************
#define KEY_UP 		0
#define KEY_DOWN	1
#define KEY_LEFT	2
#define KEY_RIGHT	3

//******PANTALLA*************
#define	WITH	560
#define	HIGH	560
//Color
#define	R	0
#define G	250
#define	B	0

//******BITMAP***************
#define BIT_X	40
#define BIT_Y	40
//color
#define BIT_R	0
#define BIT_G	250
#define BIT_B	0

//******LINEAS***************
#define LIN_X1	30
#define LIN_Y1	0
#define LIN_X2	30
#define LIN_Y2	560
#define ESPESOR	3
//Color
#define LIN_R	0
#define LIN_G	0
#define LIN_B	0

//******FICHAS***************
//Circulo
#define RADIO		(WITH/(X*4))
#define DIST		(WITH/(X*2))	//Largo de la mitad de una casilla
//Jugador 1
#define J1	1

#define CIR_J1_R	255
#define CIR_J1_G	0
#define CIR_J1_B	0

//Jugador 2
#define J2	2
#define CIR_J2_R	0
#define CIR_J2_G	0
#define CIR_J2_B	255
//Vacio
#define VACIO	0

//******TABLERO**************


/*#############PANTALLA###############*/
#define FWIDTH 640
#define FHIGH 480
/*#############FUENTE###############*/
#define NOMBRE_FUENTE "Quincy-Regular.otf"
#define TAM_LETRA 200
/*#############COLOR DE LA PANTALLA###############*/
#define FRED 0
#define FGREEN 0
#define FBLUE 0
/*#############TEXTO PARA LA PANTALLA###############*/
#define TEXTO "GANO ROJO" 
/*#############Color del texto###############*/
#define FR 255
#define FG 255
#define FB 255


int act_jugada(int *tablero, ALLEGRO_DISPLAY *display, int jugador);
void crear_tablero();
void lineas();
void act_fichas(int *tablero);
void registros_eventos(ALLEGRO_DISPLAY *display, ALLEGRO_TIMER *timer, ALLEGRO_EVENT_QUEUE *evento_cola);
void al_seleccionar(ALLEGRO_EVENT_QUEUE *evento_cola, ALLEGRO_EVENT evento,int *x,int *y,int *tablero,	int jugador);
int al_mover(ALLEGRO_EVENT_QUEUE *evento_cola, ALLEGRO_EVENT evento,int x,int y, int *tablero,	int jugador);
int al_finalizar(int *tablero, int jugador);
