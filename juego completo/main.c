#include "stdio.h"
#include "funciones_allegro.h"
#include "logica.h"

int main (int cant, char**arg)
{	
	char puerto[5];
	int op;

	printf("\n0 - Jugar en la misma PC");
	printf("\n1 - Jugar en LAN con jugador 1 (servidor)");
	printf("\n2 - Jugar en LAN con jugador 2 (cliente)\n");
	scanf("%d",&op);

	switch (op)
	{
		case 0 :
			partida();
			break;
		case 1 :
			jugador_1();
			break;
		case 2 :
			printf("\nIngrese el puerto: ");
			scanf("%s",puerto);
			jugador_2(puerto);
			break;
	}
	return 0;
}
