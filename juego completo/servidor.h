//ENCABEZADO PARA SOCKETS
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
//ENCABEZADOS STANDARD
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

//DEFINICIONES
//Sockets
#define BACKLOG 3
#define MAXPORT 60000
//Tablero
#define YMAX	4
#define XMAX	4
#define J1	1
#define J2	2
#define VACIO	0

int sock_servidor(int *puerto, struct sockaddr_in *configuracion); //Crea el socket y rellana la estructura, luego llama a bind, y listen. 									     Retorna descriptor del servidor
int mod_sockaddr(int *port, struct sockaddr_in *config);	   //Modifico la estructura con el nuevo puerto
int datos_1(int sockaddr);	//Para el intercambio de datos
