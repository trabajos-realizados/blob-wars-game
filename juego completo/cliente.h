//ENCABEZADO PARA SOCKETS
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
//ENCABEZADOS STANDARD
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//Tablero
#define YMAX	4
#define XMAX	4
#define J1	1
#define J2	2
#define VACIO	0
int conexion(int , struct sockaddr_in *);		//Crea socket y retorna el descriptor del cliente
int datos_2(int sockaddr);		//Envio de datos
