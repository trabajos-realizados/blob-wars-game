#include "servidor.h"

#define JUGADOR_ACTUAL	J1

/////////////////CONEXION/////////////////////////////////////////////////////////////////////////////////
/*
	Crea el socket y vincula la IP y el puerto. Esta funcion retorna el descriptor del servidor
*/
int sock_servidor(int *puerto, struct sockaddr_in *configuracion)
{
	int fd_servidor;	//Descriptor del servidor
	int on=1, estado;	//para setsockopt

	//CREO SOCKET
	fd_servidor=socket(AF_INET,SOCK_STREAM,0);
	if(fd_servidor==-1)
	{
		perror("ERROR EN SOCKET: ");
		exit(1);
	}

	/*Si el servidor se cierra bruscamente queda ocupado el puerto 
	y se debe reiniciar el servidor, con setsockp se soluciona */
	if((setsockopt(fd_servidor, SOL_SOCKET, SO_REUSEADDR,&on, sizeof(int))) == -1)
	{
	   	perror("SETSOCKOPT: ");
	    	exit(1);
	}

	*puerto=1;	//Inicializo
	//RELLENO LA ESTRUCTURA
	configuracion->sin_family=AF_INET;	   //Familia de direcciones
	configuracion->sin_addr.s_addr=INADDR_ANY; //Direccion local
	configuracion->sin_port=htons(*puerto);	   //Eligo el puerto
	memset(configuracion->sin_zero,0,8);	   //Se rellena a 0 para preservar tamaño original de struct sockadr

	//ASOCIAR CON IP Y PUERTO
	do
	{
		estado=bind(fd_servidor, (struct sockaddr *)configuracion,sizeof(struct sockaddr));
		if(estado==-1)
		{
			mod_sockaddr(puerto, configuracion);	//Modifico el puerto hasta que encuentre una desocupado
		}
	}while(estado==-1);

	//ESCUCHAR CONEXIONES
	if( listen(fd_servidor,BACKLOG) ==-1)	//Se pone en la la espera de conexiones
	{
		perror("ERROR DE LISTEN()\n");
		close(fd_servidor);
		exit(1);
	}

	return fd_servidor;	//Retorna el descriptor del socket servidor
}
