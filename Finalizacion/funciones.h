#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

/*#############PANTALLA###############*/
#define WIDTH 640
#define HIGH 480
/*#############FUENTE###############*/
#define NOMBRE_FUENTE "Quincy-Regular.otf"
#define TAM_LETRA 200
/*#############COLOR DE LA PANTALLA###############*/
#define RED 0
#define GREEN 0
#define BLUE 0
/*#############TEXTO PARA LA PANTALLA###############*/
#define TEXTO "GANO ROJO" 
/*#############Color del texto###############*/
#define R 255
#define G 255
#define B 255
/*############Condicion Finalizacion###########*/
#define GANO_J1  1
#define GANO_J2  2
#define EMPATE 	 3
