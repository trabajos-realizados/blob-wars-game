#include "funciones.h"

int main(int argc, char **argv){
   int resultado;

   if(!al_init()){
      fprintf(stderr, "Failed to initialize Allegro.\n");
      return -1;
   }

   al_init_font_addon(); // inicialicializa para poder usar las fuentes de letras
   al_init_ttf_addon();// verifica si existe la fuente
   ALLEGRO_DISPLAY *display = al_create_display(WIDTH,HIGH);//Crea la pantalla (ANCHO,ALTO)
   
   if (!display){
      fprintf(stderr, "No se pudo crear la pantalla.\n");
      return -1;
   }

   ALLEGRO_FONT *font;//Ingreso el tipo de fuente (Nombre de la fuente,alto de la letra,bandera)


   if (!font){
      fprintf(stderr, "No se pudo encontrar %s.\n",NOMBRE_FUENTE);
      return -1;
   }
   
   printf("Ingrese un valor: ");
   scanf("%d",&resultado);

   switch(resultado){
		case GANO_J1: {
			font = al_load_ttf_font(NOMBRE_FUENTE,150,0 );
	    	al_clear_to_color(al_map_rgb(RED,GREEN,BLUE));//Le da el color a la pantalla(RED,GREEN,BLUE)
   			al_draw_text(font, al_map_rgb(0,0,255), WIDTH/2, (HIGH/10),ALLEGRO_ALIGN_CENTRE, "Gano");//Muestra el color del texto
   			al_draw_text(font, al_map_rgb(0,0,255), WIDTH/2, (HIGH/2),ALLEGRO_ALIGN_CENTRE, "Jugador azul");
		
		break;	
		}
		case GANO_J2: {
			font = al_load_ttf_font(NOMBRE_FUENTE,150,0 );
			al_clear_to_color(al_map_rgb(RED,GREEN,BLUE));//Le da el color a la pantalla(RED,GREEN,BLUE)
   			al_draw_text(font, al_map_rgb(255,0,0), WIDTH/2, (HIGH/10),ALLEGRO_ALIGN_CENTRE, "Gano" );//Muestra el color del texto		
   			al_draw_text(font, al_map_rgb(255,0,0), WIDTH/2, (HIGH/2),ALLEGRO_ALIGN_CENTRE, "Jugador Rojo");
		
		break;
		}
		case EMPATE: {
			font = al_load_ttf_font(NOMBRE_FUENTE,250,0 );
			al_clear_to_color(al_map_rgb(RED,GREEN,BLUE));//Le da el color a la pantalla(RED,GREEN,BLUE)
   			al_draw_text(font, al_map_rgb(255,255,255), WIDTH/2, (HIGH/4),ALLEGRO_ALIGN_CENTRE,"EMPATE");//Muestra el color del texto
		
		break;
		}
	}
   
   al_flip_display();

   al_rest(3.0);

   al_destroy_display(display);

   return 0;
}
