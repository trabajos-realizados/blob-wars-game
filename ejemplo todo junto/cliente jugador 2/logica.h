#include <stdio.h>
#include <stdlib.h>

//***********	ESTADO DE CASILLA ***************
#define JUGADOR_ACTUAL	J2
#define J1	1	//Jugador 1
#define	J2	2	//Jugador 2
#define VACIO	0	//Casilla vacia
//*********TABLERO******************
#define X	8
#define Y	8
//*************CONDICIÓN*******************
#define EN_JUEGO 0
#define GANO_J1  1
#define GANO_J2  2
#define EMPATE 	 3


void init(int *tab);
void MostrarTablero (int *tab);
int seleccionar(int *,int *, int *, int J);
int mover(int *tab, int xf, int yf, int x, int y, int J);
void jugada (int *tab, int J);
void consecuencia(int *, int xf, int yf, int J);
int condicion (int *tab, int jugador);
void CambiarTurno(int *tab, int *J, int *T);
int MovimientosPosibles (int *tab, int x, int y, int jugador);
void finalizar (int *tab);

