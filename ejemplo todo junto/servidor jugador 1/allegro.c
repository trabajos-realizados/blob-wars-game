#include "funciones_allegro.h"
#include "logica.h"

void act_fichas(int *tablero)
{
	ALLEGRO_COLOR color1,color2;
	int aux,i;

	for(aux=0;aux<Y;aux++)	//Recorro la matriz
	{
		for(i=0;i<X;i++)
		{
			if(*(tablero+X*aux+i)==J1)	//Lleno con las fichas de jugador 1
			{
				color1= al_map_rgb( CIR_J1_R, CIR_J1_G, CIR_J1_B);	//Elijo color
				al_draw_filled_circle(DIST*(i*2+1), DIST*(aux*2+1), RADIO, color1);	//Dibujo circulo
			}
			if(*(tablero+X*aux+i)==J2)	//Lleno con las fichas de jugador 2
			{
				color2=al_map_rgb( CIR_J2_R, CIR_J2_G, CIR_J2_B);	//Elijo color
				al_draw_filled_circle(DIST*(i*2+1), DIST*(aux*2+1), RADIO, color2);	//Dibujo circulo
			}
		}
	}
}
/////////////////////////////////////////////////////////////////////////
int act_jugada(int *tablero, ALLEGRO_DISPLAY *display, int jugador)
{
	ALLEGRO_EVENT evento;	
	ALLEGRO_EVENT_QUEUE *evento_cola = NULL;	//Cola de eventos
	ALLEGRO_TIMER *timer = NULL;	//Temporizador
	bool key[4] = { false, false, false, false };
	bool redraw = true;
	float cx=DIST;
	float cy=DIST;
	int x=0, y=0, estado=0;
	int *xfi, *yfi;
	
	timer = al_create_timer(TIME);
	if(!timer)
	{
		perror("ERROR DE TIMER: ");
		return -1;
	}
	evento_cola = al_create_event_queue();
	if(!evento_cola)
	{
		perror("ERROR DE EVENT QUEUE: ");
		al_destroy_display(display);
		al_destroy_timer(timer);
		return -1;
	}

	registros_eventos(display, timer, evento_cola); //Asocio enventos

	crear_tablero();
	act_fichas(tablero);	//Actualizo las fichas
 	al_flip_display();

 	al_start_timer(timer);	//Arranco el temporizador

	while(estado!=1)
	{
		al_seleccionar(evento_cola,evento,&x,&y,tablero, jugador);
		estado=al_mover(evento_cola,evento,x,y, tablero, jugador);
		al_flip_display();
	}
	
	al_destroy_timer(timer);
	al_destroy_event_queue(evento_cola);

	return 0;
}
///////////////////////////////////////////////////////////////////////////
int al_mover(ALLEGRO_EVENT_QUEUE *evento_cola, ALLEGRO_EVENT evento,int x,int y, int *tablero, int jugador)
{
	float cy=DIST+2*DIST*y, cx=DIST+2*DIST*x;	//Variables para cambiar la posicion del selector
	bool redraw=true;
	bool key[4] = { false, false, false, false };	//Para guardar los estados del teclado
	int res=-1;
	int sx,sy;	//Para poner el centro del circulo
	int xf,yf;	//Coordenadas finales

	sx=cx;	//Guardo coordenadas
	sy=cy;
	xf=x;	
	yf=y;

	while(res==-1)
	{
		al_wait_for_event(evento_cola, &evento);	//Espero hasta que ocurra un evento
		if(evento.type == ALLEGRO_EVENT_TIMER)		//Evento del temporizador
		{
			if((key[KEY_UP]) && (cy>DIST) && ((yf-y)>-2))
			{
				cy -= 2*DIST;
				yf--;
			}
			if(key[KEY_DOWN] && (cy<(HIGH - DIST)) && ((yf-y)<2))
			{
				cy += 2*DIST;
				yf++;
			}
			if(key[KEY_LEFT] && (cx>DIST) && ((xf-x)>-2))
			{
				cx -= 2*DIST;
				xf--;
			}
			if(key[KEY_RIGHT] && (cx<(WITH - DIST)) && ((xf-x)<2))
			{
				cx += 2*DIST;
				xf++;
			}
			redraw = true;
		}
		if(evento.type == ALLEGRO_EVENT_KEY_DOWN)	//Al presionar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
	 				key[KEY_UP] = true;
		      			break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = true;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = true;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = true;
					break;
			}
		}
		if(evento.type == ALLEGRO_EVENT_KEY_UP)		//Al soltar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
					key[KEY_UP] = false;
					break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = false;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = false;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = false;
					break;
				case ALLEGRO_KEY_ENTER:
					res = mover(tablero,xf,yf,x,y,jugador);	//Valido el movimiento
					printf("\nCOORDENADAS: %d;%d \n",xf,yf);
					break;
			}
		}
		if(redraw && al_is_event_queue_empty(evento_cola))	//Dibujo el selector
		{
			redraw = false;
			crear_tablero();
			act_fichas(tablero);
			al_draw_circle(sx, sy, SELECTOR, al_map_rgb( ELEG_R, ELEG_G, ELEG_B), SELEC_ESP*2);	//Ficha elegida
			al_draw_circle(cx, cy, SELECTOR, al_map_rgb(SEL_R,SEL_G,SEL_B), SELEC_ESP);	//Selector
			al_flip_display();
		}
	}
	if (res == 1) {consecuencia(tablero, xf, yf, jugador);}
	return res;
}
//////////////////////////////////////////////////////////////
void al_seleccionar(ALLEGRO_EVENT_QUEUE *evento_cola, ALLEGRO_EVENT evento,int *x,int *y, int *tablero, int jugador)
{
	float cy, cx;
	if (jugador == 1)
	{
		cy=DIST;
		cx=DIST;	//Variables para cambiar la posicion del selector
		*x=0;	//Se inicializa las coordenadas
		*y=0;
	}
	else if (jugador == 2)
	{
		cy=DIST+2*DIST*(Y-1);
		cx=DIST+2*DIST*(X-1);	//Variables para cambiar la posicion del selector
		*x=(X-1);	//Se inicializa las coordenadas
		*y=(Y-1);
	}
	bool redraw=true;
	bool key[4] = { false, false, false, false };	//Para guardar los estados del teclado
	bool res=true;
	int selec;

	

	while(res)
	{
		al_wait_for_event(evento_cola, &evento);	//Espero hasta que ocurra un evento
		if(evento.type == ALLEGRO_EVENT_TIMER)		//Evento del temporizador
		{
			if(key[KEY_UP] && cy > DIST)
			{
				cy -= 2*DIST;
				(*y)--;
			}
			if(key[KEY_DOWN] && cy < HIGH - DIST)
			{
				cy += 2*DIST;
				(*y)++;
			}
			if(key[KEY_LEFT] && cx > DIST)
			{
				cx -= 2*DIST;
				(*x)--;
			}
			if(key[KEY_RIGHT] && cx < WITH - DIST)
			{
				cx += 2*DIST;
				(*x)++;
			}
			redraw = true;
		}
		if(evento.type == ALLEGRO_EVENT_KEY_DOWN)	//Al presionar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
	 				key[KEY_UP] = true;
		      			break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = true;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = true;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = true;
					break;
			}
		}
		if(evento.type == ALLEGRO_EVENT_KEY_UP)		//Al soltar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
					key[KEY_UP] = false;
					break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = false;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = false;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = false;
					break;
				case ALLEGRO_KEY_ENTER:
					selec = seleccionar(tablero, x, y, jugador);	//Valido si eligio bien la ficha
					if(selec==0)	//Si eligio bien
					{
						res = false;
					}
					break;
			}
		}
		if(redraw && al_is_event_queue_empty(evento_cola))	//Dibujo el selector
		{
			redraw = false;
			crear_tablero();
			act_fichas(tablero);
			al_draw_circle(cx,cy, SELECTOR,al_map_rgb(SEL_R,SEL_G,SEL_B),SELEC_ESP);
			al_flip_display();
		}
	}
}
////////////////////////////////////////////////////////////////////////////
void registros_eventos(ALLEGRO_DISPLAY *display, ALLEGRO_TIMER *timer, ALLEGRO_EVENT_QUEUE *evento_cola)
{
	ALLEGRO_EVENT_SOURCE *evento_display, *evento_timer, *evento_keyboard;

	//Se optienen los origenes de eventos
	evento_display=al_get_display_event_source(display);	//Evento de pantalla
	evento_timer=al_get_timer_event_source(timer);		//Evento de temporizador
	evento_keyboard=al_get_keyboard_event_source();		//Evento de teclado

	//Se asocian los eventos y se guardan en la cola
	al_register_event_source(evento_cola, evento_display);
	al_register_event_source(evento_cola, evento_timer);
	al_register_event_source(evento_cola, evento_keyboard);
}
//////////////////////////////////////////////////////////////////////////////
void crear_tablero()
{
	ALLEGRO_COLOR color;

	color=al_map_rgb(R,G,B);	//Elijo color
	al_clear_to_color(color);	//Le doy color a la pantalla

	lineas();	//Genero lineas
}
/////////////////////////////////////////////////////////////////////////////
void lineas()
{
	ALLEGRO_COLOR color;
	int aux, x, y;

	x=WITH/X;	//Calculo distacia entre lineas
	y=HIGH/Y;
	
	for(aux=0;aux<X;aux++)	//GENERO LINEAS VERTICALES
	{
		color=al_map_rgb( LIN_R, LIN_G, LIN_B);	//Elijo color
		al_draw_line(x*aux, 0, x*aux, HIGH, color, ESPESOR);	//Configuro la linea
	}

	for(aux=0;aux<Y;aux++)	//GENERO LINEAS HORIZONTALES
	{
		color=al_map_rgb( LIN_R, LIN_G, LIN_B);	//Elijo color
		al_draw_line( 0, y*aux, WITH, y*aux, color, ESPESOR);	//Configuro la linea
	}
}
