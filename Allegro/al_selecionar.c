/*
	Crea un seleccionador en el tablero. Los puntero x y es donde se guarda las coordenadas donde se a elegido la ficha.
	Para seleccionar alguna casilla se debe presionar ENTER y el selecctor se mueve con las flechas del teclado.
*/
#include "funciones_allegro.h"

void al_seleccionar(ALLEGRO_EVENT_QUEUE *evento_cola, ALLEGRO_EVENT evento,int *x,int *y, int *tablero, int jugador)
{
	float cy=DIST, cx=DIST;	//Variables para cambiar la posicion del selector
	bool redraw=true;
	bool key[4] = { false, false, false, false };	//Para guardar los estados del teclado
	bool res=true;
	int selec;

	*x=0;	//Se inicializa las coordenadas
	*y=0;

	while(res)
	{
		al_wait_for_event(evento_cola, &evento);	//Espero hasta que ocurra un evento
		if(evento.type == ALLEGRO_EVENT_TIMER)		//Evento del temporizador
		{
			if(key[KEY_UP] && cy > DIST)
			{
				cy -= 2*DIST;
				(*y)--;
			}
			if(key[KEY_DOWN] && cy < HIGH - DIST)
			{
				cy += 2*DIST;
				(*y)++;
			}
			if(key[KEY_LEFT] && cx > DIST)
			{
				cx -= 2*DIST;
				(*x)--;
			}
			if(key[KEY_RIGHT] && cx < WITH - DIST)
			{
				cx += 2*DIST;
				(*x)++;
			}
			redraw = true;
		}
		if(evento.type == ALLEGRO_EVENT_KEY_DOWN)	//Al presionar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
	 				key[KEY_UP] = true;
		      			break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = true;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = true;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = true;
					break;
			}
		}
		if(evento.type == ALLEGRO_EVENT_KEY_UP)		//Al soltar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
					key[KEY_UP] = false;
					break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = false;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = false;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = false;
					break;
				case ALLEGRO_KEY_ENTER:
					selec = seleccionar(tablero, x, y, jugador);	//Valido si eligio bien la ficha
					if(selec==0)	//Si eligio bien
					{
						res = false;
					}
					break;
			}
		}
		if(redraw && al_is_event_queue_empty(evento_cola))	//Dibujo el selector
		{
			redraw = false;
			crear_tablero();
			act_fichas(tablero);
			al_draw_circle(cx,cy, SELECTOR,al_map_rgb(SEL_R,SEL_G,SEL_B),SELEC_ESP);
			al_flip_display();
		}
	}
}
