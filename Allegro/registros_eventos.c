/*
	Se declaran los eventos a utilizar
*/
#include "funciones_allegro.h"

void registros_eventos(ALLEGRO_DISPLAY *display, ALLEGRO_TIMER *timer, ALLEGRO_EVENT_QUEUE *evento_cola)
{
	ALLEGRO_EVENT_SOURCE *evento_display, *evento_timer, *evento_keyboard;

	//Se optienen los origenes de eventos
	evento_display=al_get_display_event_source(display);	//Evento de pantalla
	evento_timer=al_get_timer_event_source(timer);		//Evento de temporizador
	evento_keyboard=al_get_keyboard_event_source();		//Evento de teclado

	//Se asocian los eventos y se guardan en la cola
	al_register_event_source(evento_cola, evento_display);
	al_register_event_source(evento_cola, evento_timer);
	al_register_event_source(evento_cola, evento_keyboard);
}
