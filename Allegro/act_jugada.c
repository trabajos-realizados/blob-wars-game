#include "funciones_allegro.h"

int act_jugada(int *tablero, ALLEGRO_DISPLAY *display, int jugador)
{
	ALLEGRO_EVENT evento;	
	ALLEGRO_EVENT_QUEUE *evento_cola = NULL;	//Cola de eventos
	ALLEGRO_TIMER *timer = NULL;	//Temporizador
	bool key[4] = { false, false, false, false };
	bool redraw = true;
	float cx=DIST;
	float cy=DIST;
	int x=0, y=0, estado=0;

	timer = al_create_timer(TIME);
	if(!timer)
	{
		perror("ERROR DE TIMER: ");
		return -1;
	}
	evento_cola = al_create_event_queue();
	if(!evento_cola)
	{
		perror("ERROR DE EVENT QUEUE: ");
		al_destroy_display(display);
		al_destroy_timer(timer);
		return -1;
	}

	registros_eventos(display, timer, evento_cola); //Asocio enventos

	crear_tablero();
	act_fichas(tablero);	//Actualizo las fichas
 	al_flip_display();

 	al_start_timer(timer);	//Arranco el temporizador

	while(!estado)
	{
		al_seleccionar(evento_cola,evento,&x,&y,tablero, jugador);
		estado=al_mover(evento_cola,evento,x,y,tablero, jugador);
		al_flip_display();
	}
	al_destroy_timer(timer);
	al_destroy_event_queue(evento_cola);

	return 0;
}
