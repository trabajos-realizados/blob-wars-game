#include "funciones_allegro.h"

int main()
{
	ALLEGRO_DISPLAY *display;
	int tablero[X][Y];
	int aux,i, jugador, turno;
		
	jugador = 1;
	turno = 1;

	if(!al_init())
	{
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}
	display = al_create_display(WITH, HIGH);
	if(!display)
	{
	      	perror("\nERROR DE PISPLAY: ");
	      	return -1;
   	}
	al_init_primitives_addon();
	if(!al_install_keyboard())
	{
		fprintf(stderr, "failed to initialize the keyboard!\n");
		return -1;
	}
		init(tablero[0]);
		MostrarTablero (tablero[0]);
		printf ("turno de jugador 1 \n");
	while((condicion(&tablero[0][0], jugador)==EN_JUEGO))
	{
		act_jugada(tablero[0],display, jugador);

		//MOSTRAR LA JUGADA DEL OTRO
		crear_tablero();
		act_fichas(tablero[0]);
		al_flip_display();
		CambiarTurno(&tablero[0][0], &jugador, &turno);
	}

	finalizar(&tablero[0][0]);	

	al_destroy_display(display);

	return 0;
}
