#include "funciones_allegro.h"
/*
	Crea un seleccionador en el tablero. Los puntero x y es donde se guarda las coordenadas donde se a elegido la ficha.
	Para seleccionar alguna casilla se debe presionar ENTER y el selecctor se mueve con las flechas del teclado.
*/
#include "funciones_allegro.h"

int al_mover(ALLEGRO_EVENT_QUEUE *evento_cola, ALLEGRO_EVENT evento,int x,int y, int *tablero, int jugador)
{
	float cy=DIST+2*DIST*y, cx=DIST+2*DIST*x;	//Variables para cambiar la posicion del selector
	bool redraw=true;
	bool key[4] = { false, false, false, false };	//Para guardar los estados del teclado
	int res=-1;
	int sx,sy;	//Para poner el centro del circulo
	int xf,yf;	//Coordenadas finales

	sx=cx;	//Guardo coordenadas
	sy=cy;
	xf=x;	
	yf=y;

	while(res==-1)
	{
		al_wait_for_event(evento_cola, &evento);	//Espero hasta que ocurra un evento
		if(evento.type == ALLEGRO_EVENT_TIMER)		//Evento del temporizador
		{
			if((key[KEY_UP]) && (cy>DIST) && ((yf-y)>-2))
			{
				cy -= 2*DIST;
				yf--;
			}
			if(key[KEY_DOWN] && (cy<(HIGH - DIST)) && ((yf-y)<2))
			{
				cy += 2*DIST;
				yf++;
			}
			if(key[KEY_LEFT] && (cx>DIST) && ((xf-x)>-2))
			{
				cx -= 2*DIST;
				xf--;
			}
			if(key[KEY_RIGHT] && (cx<(WITH - DIST)) && ((xf-x)<2))
			{
				cx += 2*DIST;
				xf++;
			}
			redraw = true;
		}
		if(evento.type == ALLEGRO_EVENT_KEY_DOWN)	//Al presionar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
	 				key[KEY_UP] = true;
		      			break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = true;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = true;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = true;
					break;
			}
		}
		if(evento.type == ALLEGRO_EVENT_KEY_UP)		//Al soltar una tecla
		{
			switch(evento.keyboard.keycode)
			{
				case ALLEGRO_KEY_UP:
					key[KEY_UP] = false;
					break;
				case ALLEGRO_KEY_DOWN:
					key[KEY_DOWN] = false;
					break;
				case ALLEGRO_KEY_LEFT: 
					key[KEY_LEFT] = false;
					break;
				case ALLEGRO_KEY_RIGHT:
					key[KEY_RIGHT] = false;
					break;
				case ALLEGRO_KEY_ENTER:
					res = mover(tablero,xf,yf,x,y,jugador);	//Valido el movimiento
					printf("\nCOORDENADAS: %d;%d",xf,yf);
					break;
			}
		}
		if(redraw && al_is_event_queue_empty(evento_cola))	//Dibujo el selector
		{
			redraw = false;
			crear_tablero();
			act_fichas(tablero);
			al_draw_circle(sx, sy, SELECTOR, al_map_rgb( ELEG_R, ELEG_G, ELEG_B), SELEC_ESP*2);	//Ficha elegida
			al_draw_circle(cx, cy, SELECTOR, al_map_rgb(SEL_R,SEL_G,SEL_B), SELEC_ESP);	//Selector
			al_flip_display();
		}
	}
	if (res == 1) { consecuencia(tablero, xf, yf, jugador);}
	return res;
}
