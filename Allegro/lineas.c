#include "funciones_allegro.h"

void lineas()
{
	ALLEGRO_COLOR color;
	int aux, x, y;

	x=WITH/X;	//Calculo distacia entre lineas
	y=HIGH/Y;
	
	for(aux=0;aux<X;aux++)	//GENERO LINEAS VERTICALES
	{
		color=al_map_rgb( LIN_R, LIN_G, LIN_B);	//Elijo color
		al_draw_line(x*aux, 0, x*aux, HIGH, color, ESPESOR);	//Configuro la linea
	}

	for(aux=0;aux<Y;aux++)	//GENERO LINEAS HORIZONTALES
	{
		color=al_map_rgb( LIN_R, LIN_G, LIN_B);	//Elijo color
		al_draw_line( 0, y*aux, WITH, y*aux, color, ESPESOR);	//Configuro la linea
	}
}
