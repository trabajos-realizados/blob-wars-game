/*
	Actualiza el tablero. Recibe como arugmento el estado de la matriz.
*/
#include "funciones_allegro.h"

void act_fichas(int *tablero)
{
	ALLEGRO_COLOR color1,color2;
	int aux,i;

	for(aux=0;aux<Y;aux++)	//Recorro la matriz
	{
		for(i=0;i<X;i++)
		{
			if(*(tablero+X*aux+i)==J1)	//Lleno con las fichas de jugador 1
			{
				color1= al_map_rgb( CIR_J1_R, CIR_J1_G, CIR_J1_B);	//Elijo color
				al_draw_filled_circle(DIST*(i*2+1), DIST*(aux*2+1), RADIO, color1);	//Dibujo circulo
			}
			if(*(tablero+X*aux+i)==J2)	//Lleno con las fichas de jugador 2
			{
				color2=al_map_rgb( CIR_J2_R, CIR_J2_G, CIR_J2_B);	//Elijo color
				al_draw_filled_circle(DIST*(i*2+1), DIST*(aux*2+1), RADIO, color2);	//Dibujo circulo
			}
		}
	}
}
