#include "logica.h"
#include <stdlib.h>

void init(int *tab)
{
	int auxx, auxy;

	for(auxy=0;auxy<Y;auxy++)	//Inicializo el tablero
	{
		for(auxx=0;auxx<X;auxx++)
		{
			*(tab+X*auxy + auxx)=VACIO;
		}
	}

	*(tab+X*0 + 0)=J1;	//Se establecen jugadores
	*(tab+X*(Y-1) + 0)=J1;
	*(tab+X*1  -1 )=J2;
	*(tab+X*(Y-1) + (X-1) )=J2;
} 
///////////////////////
void MostrarTablero (int *tab)
{
	int auxx,auxy;

	for(auxy=0;auxy<Y;auxy++)	//Recorro el tablero
	{
		for(auxx=0;auxx<X;auxx++)
		{
			printf ("%d", *(tab+X*auxy + auxx)); 
		}
		printf ("\n");
	}
}
///////////////////////
int seleccionar(int *tab,int *x, int *y, int J)
{ 

	if ( *(tab+X*(*y)+*x) != J )   // verifico si la casilla elegida contiene una ficha del jugador
	{
		return 1;	//Ficha incorrecta
	}
	else
	{
		return 0;
	} 
}
/////////////////////////////////////////////////
int mover(int *tab, int *xf, int *yf, int x, int y, int J)
{
	if (abs(*xf -x) == 0 && abs(*yf -y) == 0) //si elige mover la ficha al mismo luegar puede cambiar de ficha
	{
		printf ("elegi otra ficha \n");
		return 0;
	}
	if ( *(tab+X*(*yf)+*xf) == VACIO && (*xf < X) && (*yf < Y) && (*xf >= 0) && (*yf >= 0)  )   
	{	
		if (( abs(*xf -x) < 3 ) && ( abs(*yf -y) < 3) )  //verifico que este en el rango de 2 casillas
		{	
			*(tab+X*(*yf)+*xf) = J;			//imprimo la ficha en el lugar que indico

			if ( abs(*xf -x) == 2 || abs(*yf -y) == 2) //si se movio a 2 de distancia borro la original, sino la dejo
			{	
				*(tab+X*(y)+x) = VACIO;
			}
			
			return 1;
		}
	}
	else 
		{ 
			printf ("move bien wacho \n");  //si anduvo mal le indico que hizo mal el movimiento 
			return -1;		
		}
	
}
/////////////////////////////////////////////////
void consecuencia(int *tab, int xf, int yf, int J)
{
	int A;	
 	if (xf < (X-1))
 	{
		A = X*(yf) + xf+1;					//me muevo a la derecha
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	//si no esta la ficha del jugador y tampoco es vacia cambio la ficha
	}
	if (yf < (Y-1))
	{	
		A = X*(yf+1) + xf ;					//me muevo hacia abajo
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;} 	
	}
	if (xf > 0 )
	{
		A = X*(yf) + xf-1;					//me muevo hacia la izquierda
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}   
	}
	if (yf > 0 )
	{
		A =X*(yf-1) + xf;					//me muevo hacia arriba
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
	if (xf < (X-1) && yf < (Y-1))
 	{
		A =X*(yf+1) + (xf+1);					 //me muevo hacia la diagonal inf derecha
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}   
	}
 	if (xf > 0 && yf > 0) 
	{	
		A =X*(yf-1) + (xf-1);					//me muevo en diagonal sup-iz
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
	if (xf > 0 && yf < (Y-1))
	{
		A = X*(yf+1) + (xf-1);					//me muevo en la diagonal inf iz
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
	if (xf < (X-1) && yf > 0)
	{
		A = X*(yf-1) + (xf+1);					//me muevo en la diagonal sup derecha
		if( *(tab+A)!= J && *(tab+A)!= VACIO){ *(tab+A)= J;}	
	}
 	
	


} 
/////////////////////////////////////////////////
void jugada (int *tab, int J)
{
	int selec, mov, jugar, x, y , xf, yf;	
	do 
	{
		do	
		{
			printf ("elegi la ficha en (X;Y) : ");
			scanf ("%d %d",&x,&y);
			x =atoi (x);
			x--;
			y = atoi (y);
			y--;
			selec = seleccionar (tab, &x, &y, J);
		} while (selec ==1); 	//elijo la ficha, no sale hasta elegir una ficha bien
	
		do	
		{
			printf ("mover a (X;Y): ");
			scanf ("%d %d",&xf,&yf);
			xf--;
			yf--;
			mov = mover (tab, &xf, &yf, x, y, J);
		} while (mov ==-1);	//elijo a donde la muevo, si no hago un movimiento valido no salgo

		if(mov == 0){jugar = 1;} // si mov es 0 entonces sigo en la jugada porque voy a elegir otra ficha
		else if (mov == 1){jugar = 0;} // si mov es 1 entonces salgo
	} while (jugar) ; 	// no sale hasta haber selecionado y movido bien una ficha

	consecuencia(tab ,xf,yf, J); // al terminar de selecionar y mover se modifica el tablero con la consecuencia de la jugada
}

///////////////////////////////////////////////////////////////////////////////////////
int condicion(int *tab)
{
	int auxx,auxy;	//Para moverme en la matriz
	int p1=0,p2=0, va = 0;	//Contador de fichas de jugadores y casillas vacias
	
	for(auxx=0;auxx<X;auxx++)	
	{
		for(auxy=0;auxy<Y;auxy++)
		{
			if( *(tab+X*auxy + auxx )== J1)	//Cuento fichas de jugador 1
			{
				p1++;
			}
			if( *(tab+X*auxy + auxx )== J2)	//Cuento fichas de jugador 2
			{
				p2++;
			}
			else if ( *(tab+X*auxy + auxx )== VACIO) //cuento las casillas vacias
			{
				va++;
			}
		}
	}
	
	//VERIFICACION
	if ( va != 0 && p2 != 0 && p1 != 0 ) // si hay al menos 1 casilla vacia y 1 ficha de cada jugador entonces sigo en juego
	{	
		return EN_JUEGO;	
	}
	if(p1==p2) // si habian 0 casillas vacias y quedaron con la misma cantidad es empate
	{
		return EMPATE;
	}
	if( (p1<p2) || (p1 == 0) ) // si el jugador 1 tiene menos fichas o se quedo sin fichas gano el jugador 2
	{
		return GANO_J2;
	}
	if( (p1>p2) || (p2 == 0) ) // si el jugador 2 tiene menos fichas o se quedo sin fichas gano el jugador 1
	{
		return GANO_J1;
	}
} 
////////////////////////////////////////
void CambiarTurno (int *tab, int *jugador, int *turno)
{
	if ((condicion(tab)==EN_JUEGO) ) // si luego del turno sigo en juego entonces cambio el turno
		{
		*jugador = *jugador+*turno;
		*turno= *turno*-1;
		printf ("turno de jugador %d \n", *jugador);		   //reinicio las condiciones de jugar y mover
		}
}
////////////////////////////////////////////
void finalizar (int *tab)
{
	switch(condicion(tab))			// al terminar el juego veo como termino
	{
		case GANO_J1: printf ("gano jugador 1 \n");
				break;
		case GANO_J2: printf ("gano jugador 2 \n");
				break;
		case EMPATE: printf ("empataron wacho \n");
				break;
	}
}
//////////////////////////////////////

