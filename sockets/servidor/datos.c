#include "servidor.h"

int datos(int sockaddr)
{
	int tam_send, tam_recv, finish=1;
	int x,y, aux,i;
	int tablero[YMAX][XMAX];

	for(aux=0;aux<XMAX;aux++)
	{
		for(i=0;i<YMAX;i++)
		{
			tablero[aux][i]=VACIO;
		}
	}
	
	do
	{
		printf("\n%d%d%d%d",tablero[0][0],tablero[1][0],tablero[2][0],tablero[3][0]);
		printf("\n%d%d%d%d",tablero[0][1],tablero[1][1],tablero[2][1],tablero[3][1]);
		printf("\n%d%d%d%d",tablero[0][2],tablero[1][2],tablero[2][2],tablero[3][2]);
		printf("\n%d%d%d%d",tablero[0][3],tablero[1][3],tablero[2][3],tablero[3][3]);

		//DOY COORDENADAS
		//funcion de juego
		printf("\nEstribo coordenadas (X;Y): ");
		scanf("%d %d",&x,&y);
		
		tablero[x][y]=J1;

		printf("\n%d%d%d%d",tablero[0][0],tablero[1][0],tablero[2][0],tablero[3][0]);
		printf("\n%d%d%d%d",tablero[0][1],tablero[1][1],tablero[2][1],tablero[3][1]);
		printf("\n%d%d%d%d",tablero[0][2],tablero[1][2],tablero[2][2],tablero[3][2]);
		printf("\n%d%d%d%d",tablero[0][3],tablero[1][3],tablero[2][3],tablero[3][3]);

		//ENVIO DATOS
		tam_send=send(sockaddr,tablero,sizeof(tablero),0);
		if(tam_send>0)
		{
			printf("ENVIADO\n");
		}
		if(tam_send<0)
		{
			perror("ERROR SEND: ");
			return -1;
		}
		//FUNCION GRAFICO

		//ESPERO DATOS
		printf("ESPERANDO RESPUESTA DE CLIENTE...");
		tam_recv=recv(sockaddr,tablero,sizeof(tablero),0);
		if(tam_recv>0)
		{
			//FUNCION GRAFICA
			printf("\n%d%d%d%d",tablero[0][0],tablero[1][0],tablero[2][0],tablero[3][0]);
			printf("\n%d%d%d%d",tablero[0][1],tablero[1][1],tablero[2][1],tablero[3][1]);
			printf("\n%d%d%d%d",tablero[0][2],tablero[1][2],tablero[2][2],tablero[3][2]);
			printf("\n%d%d%d%d",tablero[0][3],tablero[1][3],tablero[2][3],tablero[3][3]);
		}
		if(tam_recv<0)
		{
			perror("ERROR RECV: ");
			return -1;
		}
		printf("\nPara salir marcar 0");
		scanf("%d",&finish);
	}while(finish!=0);
		
	return 0;
}
