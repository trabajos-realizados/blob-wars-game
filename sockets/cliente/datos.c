#include "cliente.h"

int datos(int sockaddr)
{
	int tam_send, tam_recv, finish=1;
	int x, y;
	int tablero[XMAX][YMAX];

	do
	{
		//ESPERO RESPUESTA
		printf("\nESPERANDO RESPUESTA DE SERVIDOR...");
		tam_recv=recv(sockaddr,tablero,100,0);
		if(tam_recv>0)
		{
			printf("\n%d%d%d%d",tablero[0][0],tablero[1][0],tablero[2][0],tablero[3][0]);
			printf("\n%d%d%d%d",tablero[0][1],tablero[1][1],tablero[2][1],tablero[3][1]);
			printf("\n%d%d%d%d",tablero[0][2],tablero[1][2],tablero[2][2],tablero[3][2]);
			printf("\n%d%d%d%d",tablero[0][3],tablero[1][3],tablero[2][3],tablero[3][3]);
		}
		if(tam_recv<0)
		{
			perror("ERROR RECV: ");
			return -1;
		}

		//DOY COORDENADAS
		//funcion de juego
		printf("\nEstribo coordenadas (X;Y): ");
		scanf("%d %d",&x, &y);
		
		tablero[x][y]=J2;

		printf("\n%d%d%d%d",tablero[0][0],tablero[1][0],tablero[2][0],tablero[3][0]);
		printf("\n%d%d%d%d",tablero[0][1],tablero[1][1],tablero[2][1],tablero[3][1]);
		printf("\n%d%d%d%d",tablero[0][2],tablero[1][2],tablero[2][2],tablero[3][2]);
		printf("\n%d%d%d%d",tablero[0][3],tablero[1][3],tablero[2][3],tablero[3][3]);

		//ENVIO DATOS
		tam_send=send(sockaddr,tablero,sizeof(tablero),0);
		if(tam_send>0)
		{
			printf("\nENVIADO");
		}
		if(tam_send<0)
		{
			perror("ERROR SEND: ");
			return -1;
		}

	}while(finish!=0);
		
	return 0;
}
