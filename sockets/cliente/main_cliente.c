#include "cliente.h"

int main(int cant, char **arg)
{
	struct sockaddr_in configuracion;
	int puerto, fd_cliente, fd_servidor;
	int condicion=1, tam_recv=0, tam_send=0;
	char buf[50];

	if(cant!=2)
	{
		printf("ERROR DE ARGUMENTOS\n");
		return -1;
	}

	puerto=atoi(*(arg+1));	//Convierto el string a int

	//CREO CONEXION
	fd_cliente=conexion(puerto, &configuracion);
	if(fd_cliente==-1) return -1;

	//SE CONECTA A UN EQUIPO
	fd_servidor=connect(fd_cliente,(struct sockaddr*)&configuracion, sizeof(struct sockaddr));
	if(fd_servidor==-1)
	{
		perror("ERROR DE CONNECT: ");
		close(fd_cliente);
		return -1;
	}
	printf("Conectado!\n");

	while(condicion)
	{
		//RECIVO MENSAJE
		condicion=datos(fd_cliente);
	}
	if(close(fd_cliente)==-1)
	{
		perror("ERROR CLOSE: ");
		return -1;
	}
	printf("DESCONECTADO\n");
	return 0;
}
